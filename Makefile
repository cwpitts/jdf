LATEX		:= lualatex
LATEX_OPTS	:= -shell-escape
FILENAME	:= sample
BIB		:= biber
VIEWER		:= evince

all: 1

1:
	${LATEX} ${LATEX_OPTS} ${FILENAME}
	${BIB} ${FILENAME}
	${LATEX} ${LATEX_OPTS} ${FILENAME} 2>&1 >/dev/null
	${LATEX} ${LATEX_OPTS} ${FILENAME} 2>&1 >/dev/null

2:
	${LATEX} ${LATEX_OPTS} ${FILENAME}2
	${BIB} ${FILENAME}
	${LATEX} ${LATEX_OPTS} ${FILENAME}2 2>&1 >/dev/null
	${LATEX} ${LATEX_OPTS} ${FILENAME}2 2>&1 >/dev/null

view: all
	${VIEWER} ${FILENAME}.pdf 2>&1 >/dev/null &
clean:
	rm -rf *.pdf *.out *.toc *.aux *.log *.bbl *.blg *.bcf *.xml

.PHONY: clean view
.DEFAULT_TARGET: all
