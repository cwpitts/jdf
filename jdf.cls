% Christopher W. Pitts
% This file provides a LaTeX document class that follows the Joyner Document
% Format (JDF) used by Professor Joyner at the Georgia Institute of Technology
% for his classes in the CS department.
\ProvidesClass{jdf}[2019 Joyner Document Format]

\LoadClass[letterpaper]{article}

\ExecuteOptions{11pt}

% Font packages
\RequirePackage[T1]{fontenc}
% \RequirePackage{mathpazo}
\RequirePackage[T1]{eulervm}
\RequirePackage{tgpagella}

% Margin packages
\RequirePackage[top=1.0in, bottom=1.5in, left=1.5in, right=1.5in]{geometry}
\RequirePackage{changepage}
\RequirePackage{setspace}

% Text justification packages
\RequirePackage{ragged2e}

% Section packages 
\RequirePackage{titlesec}
\RequirePackage{xstring}
\RequirePackage{graphicx}

% Caption packages
\RequirePackage{caption}
\RequirePackage{float}

% Citation packages
\RequirePackage[
bibstyle=authoryear,
dashed=false,
sorting=nyt,
natbib=true,
maxbibnames=99]{biblatex}

% Table packages
\RequirePackage{tabularx}
\RequirePackage{siunitx}
\RequirePackage{booktabs}

% Font config
\newcommand{\jdfbodyfont}[0]{%
  \fontsize{11pt}{14pt}\selectfont
}

\newcommand{\jdftablefont}[0]{%
  \fontsize{8pt}{14pt}\selectfont
}

\newcommand{\jdfcaptionfont}[0]{%
  \fontsize{11}{14}\selectfont
}

\setlength{\parskip}{8pt}

\SetSymbolFont{letters}{normal}{T1}{eulervm}{m}{sl}
\SetSymbolFont{letters}{bold}{T1}{eulervm}{m}{sl}

% Title formatting
\renewcommand{\author}[1]{%
  \def \aauthor {#1}
}

\newcommand{\email}[1]{%
  \def \eemail {#1}
}

\renewcommand{\title}[1]{%
  \def \ttitle {#1}
}

\renewcommand{\maketitle}[0]{
  \begin{center}
    {\setstretch{1.15} \fontsize{17pt}{20pt}\selectfont \ttitle}\\[0.5cm]
    {\setstretch{1.26} \jdfbodyfont \aauthor}\\[0.1cm]
    {\setstretch{1.26} \jdfbodyfont \eemail}\\[0.5cm]
  \end{center}
}

% Abstract formatting
\renewenvironment{abstract}
{%
  \begin{center}
    \begin{adjustwidth}{0.6in}{0.6in}
      \setstretch{1.26} \fontsize{11pt}{17pt}\selectfont
      \fontdimen3\font=0.2em
      \textbf{\textit{Abstract --}}
    }
    {%
    \end{adjustwidth}
  \end{center}
}

% Section formatting
\setlength{\parindent}{0pt}
\setstretch{1.26}
\jdfbodyfont
\fontdimen3\font=0.5em

\def \secfnt {\jdfbodyfont}
\titleformat{\section}
{\normalfont\secfnt\bfseries\uppercase}{\thesection}{1em}{}
\titlespacing{\section}{0pt}{8.5pt}{0pt}

\def \ssecfnt {\jdfbodyfont}
\titleformat{\subsection}
{\normalfont\ssecfnt\bfseries}{\thesubsection}{1em}{}
\titlespacing{\subsection}{0pt}{8.5pt}{0pt}

\def \sssecfnt {\jdfbodyfont}
\titleformat{\subsubsection}
{\normalfont\sssecfnt\bfseries\itshape}{\upshape\thetitle}{1em}{}
\titlespacing{\subsubsection}{0pt}{8.5pt}{0pt}

\newcommand{\subsubsubsection}[1]{
  \textbf{\textit{#1}} -- 
}

% Some shorthand for the different headings (sections)
% These macros are \h1, \h2, etc shorthands for the different headings defined
% in the JDF guide. For those who are experienced in writing in LaTeX, these
% correspond to the \section, \subsection, etc. macros, with the addition of a
% \subsubsubsection macro for heading 4
\newcommand{\h}[2]{
  \IfStrEqCase{#1}{%
    {1}{\section{#2}}
    {2}{\subsection{#2}}
    {3}{\subsubsection{#2}}
    {4}{\subsubsubsection{#2}}
    [niets]
  }
}

% Caption font
\DeclareCaptionLabelSeparator{period}{. }
\DeclareCaptionFormat{jdfformat}{\fontsize{8.5pt}{14pt}\selectfont\textbf{#1}#2#3}
\captionsetup[figure]{name={Figure}, labelsep=period, format=jdfformat}
\captionsetup[table]{name={Table}, labelsep=period, format=jdfformat}

% Bibliography
\DeclareFieldFormat{bibentrysetcount}{\mkbibparens{\mknumalph{#1}}}
\DeclareFieldFormat{labelnumberwidth}{\mkbibbrackets{#1}}

\defbibenvironment{bibliography}
  {\list
     {\printtext[labelnumberwidth]{%
    \printfield{prefixnumber}%
    \printfield{labelnumber}}}
     {\setlength{\labelwidth}{\labelnumberwidth}%
      \setlength{\leftmargin}{\labelwidth}%
      \setlength{\labelsep}{\biblabelsep}%
      \addtolength{\leftmargin}{\labelsep}%
      \setlength{\itemsep}{\bibitemsep}%
      \setlength{\parsep}{\bibparsep}}%
      \renewcommand*{\makelabel}[1]{\hss##1}}
  {\endlist}
  {\item}

\DeclareNameAlias{sortname}{last-first}

\renewcommand{\cite}[1]{ \citeauthor{#1}, \citeyear{#1}}

\renewcommand{\citep}[1]{ (\citeauthor{#1}, \citeyear{#1})}

% % Default text spacing and font
% \setstretch{1.26} \jdfbodyfont
\endinput