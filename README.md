# jdf

Joyner Document Format (JDF) class for LaTeX.

## Makefile (development only)

| Target        | Action                       |
|---------------|------------------------------|
| all (default) | 1                            |
| 1             | Compile `sample.tex`         |
| 2             | Compile `sample2.tex`        |
| clean         | Remove output files          |
| view          | all, then open with `VIEWER` |


| Variable   | Description              | Default       |
|------------|--------------------------|---------------|
| LATEX      | LaTeX compiler           | lualatex      |
| LATEX_OPTS | LaTeX compiler options   | -shell-escape |
| FILENAME   | Base filename to compile | sample        |
| BIB        | BibTeX command           | biber         |
| VIEWER     | PDF viewer               | evince        |

## Section headings
For those familiar with LaTeX, here is the mapping between section and heading:


| Section                                    | Heading   |
|--------------------------------------------|-----------|
| section                                    | Heading 1 |
| subsection                                 | Heading 2 |
| subsubsection                              | Heading 3 |
| subsubsubsection (defined in this package) | Heading 4 |

For those who prefer to use the terminology in the JDF, the `\h` macro can be use in place of `\section`:

 | Heading   | Macro          |
|-----------|----------------|
| Heading 1 | `\h{1}{Title}` |
| Heading 2 | `\h{2}{Title}` |
| Heading 3 | `\h{3}{Title}` |
| Heading 4 | `\h{4}{Title}` |

`sample1.tex` uses the `\section` macros, and `sample2.tex` uses the `\h` macros. They should produce equivalent output, as the `\h` macros are shorthand references to the `\section` equivalents.

## TODO
* Adjust blockquote margin and indentation
